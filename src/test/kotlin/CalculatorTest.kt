import io.kotlintest.TestCase
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec

class CalculatorTest : StringSpec() {

    lateinit var calculator: Calculator

    override fun beforeTest(testCase: TestCase) {
        calculator = Calculator()
    }

    init {
        "simple expressions" {
            calculator.processInput("42") shouldBe 42
            calculator.processInput("2 + 2") shouldBe 4
            calculator.processInput("2 + 2 * 2") shouldBe 6
            calculator.processInput("2 * 2 + 2") shouldBe 6
            calculator.processInput("10 * (5 + 7)") shouldBe 120
            calculator.processInput("-1 * 7") shouldBe -7
            calculator.processInput("-1 * -1") shouldBe 1
            calculator.processInput("50000 * 50000") shouldBe 2500000000
            calculator.processInput("1 - 100") shouldBe -99
        }

        "simple variables" {
            calculator.processInput("let x = 2 * 7") shouldBe null
            calculator.processInput("x + 1") shouldBe 15
            calculator.processInput("let x = x / 5") shouldBe null
            calculator.processInput("x") shouldBe 2
        }

        "complex expressions" {
            calculator.processInput("(12 + 34) * (61 - (2 * (3 + 58))) - 90 + 78 / 10 * 32 + (147 * 32)") shouldBe 2032
            calculator.processInput("5+4*3/2") shouldBe 11
        }

        "multiple variables" {
            calculator.processInput("let x = 2 + 2") shouldBe null
            calculator.processInput("let y = x - 1") shouldBe null
            calculator.processInput("x * y") shouldBe 12
            calculator.processInput("let z = x * x + y * y") shouldBe null
            calculator.processInput("z") shouldBe 25
        }

        "invalid expressions" {
            shouldThrow<CalculatorException> { calculator.processInput("1 + ") }
            shouldThrow<CalculatorException> { calculator.processInput("1 + *") }
            shouldThrow<CalculatorException> { calculator.processInput("x") }
            shouldThrow<ArithmeticException> { calculator.processInput("1 / 0") }

            shouldThrow<CalculatorException> { calculator.processInput("let") }
            shouldThrow<CalculatorException> { calculator.processInput("let x = ") }
            shouldThrow<CalculatorException> { calculator.processInput("let x 1 2") }
            shouldThrow<CalculatorException> { calculator.processInput("let + = 7") }
        }
    }

}
