class TokenOperator(val operatorType: OperatorType, val priority: Byte) : Token(TokenType.OPERATOR)
