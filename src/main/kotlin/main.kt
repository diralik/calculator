fun main() {
    val calculator = Calculator()

    while (true) {
        print(">>> ")
        System.out.flush()

        val input = readLine()
        if (input === null) {
            break
        }
        if (input.isBlank()) {
            continue
        }

        try {
            val result = calculator.processInput(input)
            if (result !== null) {
                println(result)
            }
        } catch (exception: CalculatorException) {
            println("Error: ${exception.message}")
        } catch (exception: ArithmeticException) {
            println("Error: ${exception.message}")
        }
    }
}
