object Tokens {
    val PLUS: Token = TokenOperator(OperatorType.PLUS, 1)
    val MINUS: Token = TokenOperator(OperatorType.MINUS, 1)
    val MULTIPLY: Token = TokenOperator(OperatorType.MULTIPLY, 2)
    val DIVIDE: Token = TokenOperator(OperatorType.DIVIDE, 2)
    val OPEN_BRACKET = Token(TokenType.OPEN_BRACKET)
    val CLOSE_BRACKET = Token(TokenType.CLOSE_BRACKET)
}
