enum class OperatorType {
    PLUS,
    MINUS,
    MULTIPLY,
    DIVIDE,
}
