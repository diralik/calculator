enum class TokenType {
    NUMBER,
    OPERATOR,
    OPEN_BRACKET,
    CLOSE_BRACKET,
    VARIABLE,
}
