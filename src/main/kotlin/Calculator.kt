import java.util.ArrayDeque

class Calculator {
    companion object {
        val LEXEMES_TO_TOKENS = mapOf(
            "+" to Tokens.PLUS,
            "-" to Tokens.MINUS,
            "*" to Tokens.MULTIPLY,
            "/" to Tokens.DIVIDE,
            "(" to Tokens.OPEN_BRACKET,
            ")" to Tokens.CLOSE_BRACKET
        )
        val LEXEME_REGEX = "[+*/()]".toRegex()
    }

    private val variables = HashMap<String, Long>()

    fun processInput(input: String): Long? {
        val lexemes = extractLexemes(input)
        // syntax: let x = <expression>
        if (lexemes.isNotEmpty() && lexemes[0] == "let") {
            processVariableDeclaration(lexemes)
            return null
        } else {
            return evaluateExpression(lexemes)
        }
    }

    private fun processVariableDeclaration(lexemes: List<String>) {
        if (lexemes.size < 4 || lexemes[2] != "=") {
            throw CalculatorException("Invalid syntax for declaring variable.\nExpect: let NAME = EXPRESSION")
        }
        val variableName = lexemes[1]
        if (variableName in LEXEMES_TO_TOKENS) {
            throw CalculatorException("Invalid variable name: '${variableName}'")
        }
        val variableValue = evaluateExpression(lexemes.subList(3, lexemes.size))
        variables[variableName] = variableValue
    }

    private fun evaluateExpression(lexemes: List<String>): Long {
        val tokens = extractTokens(lexemes)
        val rpn = convertToRPN(tokens)
        return evaluateRPN(rpn)
    }

    private fun extractLexemes(input: String): List<String> {
        return input
            .replace(LEXEME_REGEX, " $0 ")
            .trim()
            .split("\\s+".toRegex())
    }

    private fun extractTokens(lexemes: List<String>): List<Token> {
        return lexemes.mapIndexed { index, lexeme ->
            when {
                lexeme in LEXEMES_TO_TOKENS -> LEXEMES_TO_TOKENS[lexeme]!!
                lexeme in variables -> TokenVariable(lexeme)
                else -> {
                    val number = lexeme.toLongOrNull()
                    if (number === null) {
                        throw CalculatorException(generateInvalidTokenMessage(lexemes, index))
                    } else {
                        TokenNumber(number)
                    }
                }
            }
        }
    }

    private fun generateInvalidTokenMessage(lexemes: List<String>, index: Int): String {
        val expressionPrefix = "> "

        val totalLengthOfLexemsBefore = lexemes.subList(0, index).stream().mapToInt { it.length }.sum()
        val numberSpacesBefore = totalLengthOfLexemsBefore + /* number spaces between lexemes: */ index
        val numberArrows = lexemes[index].length

        return "Invalid token: \n" +
                expressionPrefix + lexemes.joinToString(" ") + "\n" +
                " ".repeat(numberSpacesBefore + expressionPrefix.length) + "^".repeat(numberArrows)
    }

    private fun convertToRPN(tokens: List<Token>): ArrayDeque<Token> {
        val output = ArrayDeque<Token>()
        val stack = ArrayDeque<Token>()
        for (token in tokens) {
            when (token.type) {
                TokenType.NUMBER, TokenType.VARIABLE -> output.addLast(token)
                TokenType.OPEN_BRACKET -> stack.addLast(token)
                TokenType.CLOSE_BRACKET -> {
                    while (!stack.isEmpty() && stack.peekLast().type !== TokenType.OPEN_BRACKET) {
                        output.addLast(stack.pollLast())
                    }
                    if (stack.isEmpty() || stack.peekLast().type !== TokenType.OPEN_BRACKET) {
                        throw CalculatorException("Bad brackets balance")
                    }
                    stack.pollLast()
                }
                TokenType.OPERATOR -> {
                    val priority = (token as TokenOperator).priority
                    while (!stack.isEmpty() && stack.peekLast().type === TokenType.OPERATOR
                        && priority <= (stack.peekLast() as TokenOperator).priority
                    ) {
                        output.addLast(stack.pollLast())
                    }
                    stack.addLast(token)
                }
            }
        }
        while (!stack.isEmpty()) {
            output.addLast(stack.pollLast())
        }
        if (output.isEmpty()) {
            throw CalculatorException("Empty string is not a valid expression")
        }
        for (token in output) {
            if (token.type === TokenType.OPEN_BRACKET) {
                throw CalculatorException("Bad brackets balance")
            }
        }
        return output
    }

    private fun evaluateRPN(rpn: ArrayDeque<Token>): Long {
        val stack = ArrayDeque<Long>()
        for (token in rpn) {
            when (token.type) {
                TokenType.NUMBER -> stack.push((token as TokenNumber).value)
                TokenType.VARIABLE -> {
                    val variableName = (token as TokenVariable).name
                    val variableValue = variables[variableName]!!
                    stack.push(variableValue)
                }
                TokenType.OPERATOR -> {
                    val operator = token as TokenOperator
                    if (stack.size < 2) {
                        throw CalculatorException("Not enough arguments for operator ${operator.operatorType}")
                    }
                    val operand2 = stack.pop()
                    val operand1 = stack.pop()
                    stack.push(applyOperator(operator.operatorType, operand1, operand2))
                }
            }
        }
        if (stack.size > 1) {
            throw CalculatorException("Missing operator at top level of expression")
        }
        return stack.peek()
    }

    private fun applyOperator(type: OperatorType, a: Long, b: Long): Long {
        return when (type) {
            OperatorType.PLUS -> Math.addExact(a, b)
            OperatorType.MINUS -> Math.subtractExact(a, b)
            OperatorType.MULTIPLY -> Math.multiplyExact(a, b)
            OperatorType.DIVIDE -> a / b
        }
    }
}
